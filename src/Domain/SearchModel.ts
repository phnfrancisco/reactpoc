import Subject from "../Infrastructure/utils/Subject";
import { debounce } from 'lodash';
import ItunesAPIRepository from "./ItunesSearchRepository";
import { Media } from "./Media";

export default class SearchModel {
    //@subject searchValue
    searchValueSubject = new Subject<string>('');
    set searchValue(searchValue: string) {
        this.searchValueSubject.setState(searchValue);
        this.searchValueSubject.notify(searchValue);
        this.search();
    };
    get searchValue(): string {
        return this.searchValueSubject.getState()
    }

    resultsSubject = new Subject<Media[]>([]);
    set results(results: Media[]) {
        this.resultsSubject.setState(results);
        this.resultsSubject.notify(results);
    };
    get results(): Media[] {
        return this.resultsSubject.getState()
    }

    search = debounce(async () => {
        console.log('>>> search')        
        const result = await ItunesAPIRepository.searchMusic(this.searchValue);
        this.results = result.results;
        console.log('>>> result', this.results);
    }, 800);

}

export const searchModelInstance = new SearchModel();