export type Media = {
    artistName: string;
    artworkUrl60: string;
    collectionName: string;
    description: string;
    wrapperType: string;
}