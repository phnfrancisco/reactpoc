import APIRequestService from "../Infrastructure/APIRequest";
import { Media } from "./Media";

const ITUNES_API_URL = 'https://itunes.apple.com/';

type ItunesResponse<T> = {
    resultCount: number;
    results: T[]
}

export default class ItunesAPIRepository {
    static searchMusic(term: string): Promise<ItunesResponse<Media>> {
        return APIRequestService.get(`${ITUNES_API_URL}search?term=${encodeURIComponent(term)}`)
    }
}