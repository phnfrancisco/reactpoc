import './App.css';
import Search from './components/Search';
import SearchResults from './components/SearchResults';
import logo from '../logo.svg';

function App() {


  return (
    <div className="App">
      <header className="App-header">
        <section>
          <h2>iTunes search</h2>
          <Search />
          <SearchResults />
        </section>
        
        <img src={logo} className="App-logo" alt="logo" />
        <p>
          Edit <code>src/App.tsx</code> and save to reload.
        </p>
        <a
          className="App-link"
          href="https://reactjs.org"
          target="_blank"
          rel="noopener noreferrer"
        >
          Learn React
        </a>
      </header>
    </div>
  );
}

export default App;
