import IView from "../../../Infrastructure/utils/IView";

export interface SearchHandlers {
    onChange: (event: React.ChangeEvent<HTMLInputElement>) => void
}

export interface SearchViewModel {
    searchValue: string
}

export type SearchViewProps = IView<SearchViewModel, SearchHandlers>

const SearchView = (props: SearchViewProps) => {
    const { viewModel, handlers } = props;
    return <input type="search" value={viewModel.searchValue} onChange={handlers?.onChange} />
}

export default SearchView;