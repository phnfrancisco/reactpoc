import { useMemo } from "react";
import useSubjects from "../../../Infrastructure/hooks/useSubject";
import searchPresenter from "./searchPresenter";
import SearchView from "./SearchView"


const Search = () => {
    const { viewModel, handlers } = useMemo(searchPresenter, []);
    useSubjects([viewModel.searchValueSubject]);

    return <SearchView viewModel={viewModel} handlers={handlers} />
}

export default Search;