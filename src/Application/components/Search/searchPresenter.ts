import SearchModel, { searchModelInstance } from "../../../Domain/SearchModel";
import { SearchHandlers } from "./SearchView";


const searchPresenter = (): {
    viewModel: SearchModel,
    handlers: SearchHandlers
} => {
    const viewModel = searchModelInstance;
    
    const onChange = (evt: React.ChangeEvent<HTMLInputElement>) => {
        console.log(evt.target.value)
        viewModel.searchValue = evt.target.value;
    };

    return {
        viewModel,
        handlers: {
            onChange
        }
    };
}

export default searchPresenter;