import { Media } from "../../../Domain/Media";
import IView from "../../../Infrastructure/utils/IView";

export interface SearchResultsViewModel {
    results: Media[]
}

export type SearchResultsViewProps = IView<SearchResultsViewModel>

const SearchResult = (media: Media) => {
    return (
        <div className="card">
            <h3>{media.wrapperType} - {media.collectionName}</h3>
            <h4>{media.artistName}</h4>
            <img src={media.artworkUrl60} />
            <p>{media.description}</p>
        </div>
    );
}

const SearchResultsView = ({ viewModel }: SearchResultsViewProps) => {
    return (
        <div className="search-results">
            { viewModel.results.map(SearchResult) }
        </div>
    );
}

export default SearchResultsView;