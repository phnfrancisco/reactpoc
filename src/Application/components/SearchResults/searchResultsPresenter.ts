import SearchModel, { searchModelInstance } from "../../../Domain/SearchModel";

const searchResultsPresenter = (): {
    viewModel: SearchModel
} => {
    const viewModel = searchModelInstance;

    return {
        viewModel
    };
}

export default searchResultsPresenter;