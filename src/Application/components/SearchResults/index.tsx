import { useMemo } from "react";
import useSubjects from "../../../Infrastructure/hooks/useSubject";
import searchResultsPresenter from "./searchResultsPresenter";
import SearchResultsView from "./SearchResultsView";
import "./index.css";


const SearchResults = () => {
    const { viewModel } = useMemo(searchResultsPresenter, []);
    useSubjects([viewModel.resultsSubject]);

    return <SearchResultsView viewModel={viewModel} />
}

export default SearchResults;