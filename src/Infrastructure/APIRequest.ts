export default class APIRequestService {
    static get<T>(url: string): Promise<T> {
        return new Promise((resolve, reject) =>  {
            fetch(url)
            .then(response => {
                resolve(response.json());
            })
            .catch(reject);
        })
    }
}