import { useEffect, useState } from "react";
import Observer from "../utils/Observer";
import Subject from "../utils/Subject";

function useSubjects(subjects?: Subject<any>[]) {
    const [, update] = useState(0);

    useEffect(() => {
        const updateState = () => {
            update(Date.now());
        }
        const observer = new Observer(updateState);
        subjects?.forEach(subject => {
            subject.attach(observer);
        });
        return () => subjects?.forEach(subject => {
                subject.detach(observer);
            });
    }, [subjects]);
}

export default useSubjects