import type Observer from "./Observer";

class Subject<S> {
    private observers: Observer[] = [];

    constructor(private state: S) {}

    public setState(state: S) {
        this.state = state;
    }

    public getState() {
        return this.state
    }
    
    public attach(observer: Observer) {
        this.observers.push(observer);
    }

    public detach(observer: Observer) {
        const observerIndex = this.observers.findIndex(o => o === observer);
        if (observerIndex !== -1) this.observers.slice(observerIndex, 1);
    }

    public notify<S>(state: S) {
        this.observers.forEach(observer => observer.update(state));
    }
}

export default Subject;