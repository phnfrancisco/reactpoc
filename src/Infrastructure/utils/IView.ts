interface IView<TModel, THandlers = {}> {
    viewModel: TModel;
    handlers?: THandlers;
}

export default IView;