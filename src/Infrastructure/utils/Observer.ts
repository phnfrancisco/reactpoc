export default class Observer {
    constructor(public update: <S>(state: S) => void) {}
}